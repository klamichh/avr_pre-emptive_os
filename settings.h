/**
 * OS Settings
 *
 */
#ifndef _OS_SETTINGS_H_
#define _OS_SETTINGS_H_

#define IDLE_TASK_STACK 64
#define MAX_TASKS 4

#define SEMAPHORE
#define MAX_SEMAPHORES 4

#define QUEUE
#define MAX_QUEUES 4

#endif //_OS_SETTINGS_H_


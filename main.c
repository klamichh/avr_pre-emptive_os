/**
 * Main file for OS demo
 */

#include "OS.h"

#include <avr/io.h>
#include <avr/interrupt.h>


static Semaphore *sem;

static uint8_t val;

static uint8_t st[128];
void the_task(void)
{
    TCCR0B |= (1 << CS00);
    TIMSK0 |= (1 << TOIE0);
    while (1)
    {
        semaphore_pend(sem);
        TCCR0B = 0;

        val++;
    }
}

int main(void)
{
    init();

    sem = semaphore_init(0);

    new_task(&the_task, &st[127]);

    run();

    return 0;
}

ISR(TIMER0_OVF_vect)
{
    isr_enter();
    semaphore_post(sem);
    isr_exit();
}

